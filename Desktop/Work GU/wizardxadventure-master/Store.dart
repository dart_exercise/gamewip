import 'dart:io';

import 'Object.dart';

class Store extends Object {
  late int x, y;
  Store(int x, int y) : super("S", x, y) {
    this.x = x;
    this.y = y;
  }

  String showStore() {
    print("\nStore :");
    print("1. Upgrade 100 HP (50c)");
    print("2. Upgrade Fire ball increase 3dmg. (20c)");
    print("3. Upgrade Thunder bolt increase 10dmg. (25c)");
    print("4. Buy HP Potion (100c)");
    print("5. Exit");
    var input = stdin.readLineSync()!;
    return input;
  }
}
