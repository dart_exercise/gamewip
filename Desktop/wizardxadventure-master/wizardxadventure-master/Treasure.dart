import 'Object.dart';

class Treasure extends Object {
  late int x, y;
  Treasure(int x, int y) : super("H", x, y) {
    this.x = x;
    this.y = y;
  }
}
