import 'dart:io';
import 'Game.dart';
import 'Monster.dart';
import 'Store.dart';
import 'TableMap.dart';
import 'Object.dart';
import 'package:chalkdart/chalk.dart';

class Wizard extends Object {
  late TableMap map;
  late Store store = new Store(0, 0);
  late Game game = Game();
  late Monster monster = new Monster(0, 0, 0);
  late int x, y;
  late int HP = 150;
  late int defaultHP = 150;
  late int dmg1 = 5;
  late int dmg2 = 10;
  late String name;
  late int cash = 0;
  late int potions = 0;
  late int count;
  Wizard(String symbol, int x, int y, TableMap map) : super(symbol, x, y) {
    this.symbol = symbol;
    this.x = x;
    this.y = y;
    this.map = map;
  }

  //walk
  bool walk(String direction, map) {
    switch (direction) {
      case 'W':
      case 'w':
        if (walkN()) {
          return false;
        }
        break;
      case 'S':
      case 's':
        if (walkS()) {
          return false;
        }
        break;
      case 'D':
      case 'd':
        if (walkE()) {
          return false;
        }

        break;
      case 'A':
      case 'a':
        if (walkW()) {
          return false;
        }
        break;
      default:
        return false;
    }
    // check();
    return true;
  }

  int getHPWiz() {
    return this.HP;
  }

  void setHPWiz(int HP) {
    this.HP = HP;
  }

  int getHPDefault() {
    return this.defaultHP;
  }

  void setHPDefault(int defaultHP) {
    this.defaultHP = defaultHP;
  }

  int getDmg1() {
    return this.dmg1;
  }

  int getDmg2() {
    return this.dmg2;
  }

  int getPotions() {
    return this.potions;
  }

  int getCash() {
    return this.cash;
  }

  void setPotions(int potions) {
    this.potions = potions;
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y) && !map.isStar(x, y);
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  String nameWizard(name) {
    this.name = name;
    print("\nNice to meet you $name!!");
    var key = stdin.readLineSync()!;
    print(chalk.red("""
 ██     ██ ████████ ██      ███████  ██████  ███       ███ ████████     ████████  ██████     
 ██     ██ ██       ██     ██       ██    ██ ████     ████ ██              ██    ██    ██    
 ██  █  ██ ███████  ██     ██       ██    ██ ██ ██   ██ ██ ███████         ██    ██    ██   
 ██ ███ ██ ██       ██     ██       ██    ██ ██  ██ ██  ██ ██              ██    ██    ██     
  ███ ███  ████████ ██████  ███████  ██████  ██   ███   ██ ████████        ██     ██████ 
    """));

    print(chalk.red("""
██     ██ ██████ ████████ ████████  ███████  ██████       ██     ██  ██████   ███████  ██     ██████   ██
██     ██   ██        ██  ██    ██  ██    ██ ██    ██     ██     ██ ██    ██  ██    ██ ██     ██    ██ ██
██  █  ██   ██      ███   ████████  ████████ ██    ██     ██  █  ██ ██    ██  ████████ ██     ██    ██ ██
██ ███ ██   ██    ██      ██    ██  ██  ██   ██    ██     ██ ███ ██ ██    ██  ██  ██   ██     ██    ██
 ███ ███  ██████ ████████ ██    ██  ██  ████ ██████        ███ ███   ██████   ██  ████ ██████ ██████   ██
    """));
    print("\nHow to play ?");
    print("1. Walk key is 'w' 'a' 's' 'd' and enter");
    print("2. 'W' is your character");
    print("3. 'H' is Treasure (you will get cash when founded)");
    print("4. 'S' is Store (you can buy item and upgrade your status)");
    print("5. 'X' is Monster (fight for receive cash)");
    print("6. 'B' is Boss (last monster before clear this game)");
    print("7. '>' is exit door");
    print("\nAre you ready ?");
    print("\nPress Enter to continue..");
    key = stdin.readLineSync()!;
    print("Let's play!");
    print("\n[short story]");
    print(
        "...You are a wizard of the village, But then one day the dragon came to attack the village.");
    print(
        "So a hero who has the ability to fight with Dragon must travel to kill it!");
    print("\nWhich that person is you! $name!! hahaaaa");
    key = stdin.readLineSync()!;
    print("\n");
    return name;
  }

  String getName() {
    return this.name;
  }

  String showSkills(int count) {
    print("Skill :");
    print("1. Fire ball");
    print("2. Thunder bolt [total $count]");
    print("3. back");
    String input = stdin.readLineSync()!;
    return input;
  }

  String showPotions() {
    print("You have $potions bottles");
    print("Do you want to use potions?");
    print("1. Yes");
    print("2. No");
    String choose = stdin.readLineSync()!;
    return choose;
  }

  void checkBag() {
    print("\n$name BAG");
    print("Cash $cash");
    print("HP $HP");
    print("Fire ball $dmg1 dmg.");
    print("Thunder bolt $dmg2 dmg.");
    print("Potions have $potions");
    print("\nPress Enter to continue..");
    var key = stdin.readLineSync()!;
  }

  void checkStore() {
    var input = "";
    if (map.isStore(x, y) && input != "5" ||
        (x == 5 && y == 1) ||
        (x == 27 && y == 3) ||
        (x == 5 && y == 4) ||
        (x == 22 && y == 6)) {
      print("Your cash : $cash");
      input = store.showStore();
      if (input == "1" && cash >= 50) {
        HP += 100;
        //DefaultHP
        defaultHP += 100;
        cash -= 50;
        checkStore();
      } else if (input == "2" && cash >= 20) {
        dmg1 += 3;
        cash -= 20;
        checkStore();
      } else if (input == "3" && cash >= 25) {
        dmg2 += 10;
        cash -= 25;
        checkStore();
      } else if (input == "4" && cash >= 100) {
        potions += 1;
        cash -= 100;
        checkStore();
      } else if (input == "5") {
        print("Exit Store");
        var key = stdin.readLineSync()!;
      } else {
        print("You don't have enough cash to buy.");
        var key = stdin.readLineSync()!;
        checkStore();
      }
    }
  }

  void checkTreasure() {
    if (map.isTreasure(x, y) ||
        (x == 4 && y == 1) ||
        (x == 25 && y == 2) ||
        (x == 6 && y == 3) ||
        (x == 19 && y == 3) ||
        (x == 21 && y == 3) ||
        (x == 1 && y == 6)) {
      int treasure = map.reCash(x, y);
      if (treasure > 0) {
        print("Receive 200cash");
        cash += treasure;
        //new set h
        // Treasure treasureh = Treasure("h", x, y, 0);
        // map.setTreasure(treasureh);
        var key = stdin.readLineSync()!;
      } else {
        print("This treasure is empty");
        var key = stdin.readLineSync()!;
      }
    }
  }

  bool checkMonster(int count) {
    if (map.isMonster(x, y) ||
        (x == 10 && y == 1) ||
        (x == 13 && y == 2) ||
        (x == 26 && y == 2) ||
        (x == 17 && y == 3) ||
        (x == 20 && y == 3) ||
        (x == 7 && y == 4) ||
        (x == 26 && y == 4) ||
        (x == 4 && y == 5) ||
        (x == 11 && y == 5) ||
        (x == 21 && y == 5) ||
        (x == 24 && y == 6)) {
      int hpMonster = map.checkMonsterHP(x, y);
      if (hpMonster > 0) {
        print("\nFound MONSTER!!");
        print("\n[...Entered to Battle...]");
        bool status = map.attackMonster(x, y, count, hpMonster);
        if (status != true) {
          return false;
        } else {
          return true;
        }
      } else if (map.monsterl(x, y) == "l") {
        print("This monster already died");
        var key = stdin.readLineSync()!;
      }
    }
    return false;
  }

  bool checkBoss(int count) {
    if (map.isBoss(x, y)) {
      int hpBoss = map.setBoss_d(x, y);
      if (hpBoss > 0) {
        print("D..Dra...Dragon!");
        print("\nInto the Last Round");
        print("\n[...Entered to Boss Fight...]");
        bool status = map.attackBoss(x, y, count, hpBoss);
        if (status != true) {
          return false;
        } else {
          return true;
        }
      } else if (map.boss_d(x, y) == "d") {
        print("Boss already died");
        var key = stdin.readLineSync()!;
      }
    }
    return false;
  }

  bool checkFinish(bool win) {
    if (map.isFinish(x, y)) {
      print(chalk.yellow(""" 

      
██    ██  ██████  ██    ██ 
 ██  ██  ██    ██ ██    ██ 
  ████   ██    ██ ██    ██ 
   ██    ██    ██ ██    ██ 
   ██     ██████   ██████ 


██     ██  ██████  ███    ██  ██  
██     ██ ██    ██ ████   ██  ██ 
██  █  ██ ██    ██ ██ ██  ██  ██  
██ ███ ██ ██    ██ ██  ██ ██  
 ███ ███   ██████  ██   ████  ██ 


      """));
      var key = stdin.readLineSync()!;
      return true;
    } else {
      return false;
    }
  }
}
