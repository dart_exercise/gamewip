import 'Object.dart';

class Boss extends Object {
  late int x, y;
  late int HP = 1500;
  late int atk = 90;
  Boss(int x, int y) : super("B", x, y) {
    this.x = x;
    this.y = y;
  }

  int getHP() {
    return this.HP;
  }

  int getAtk() {
    return this.atk = atk;
  }

  void setAtk(int atk) {
    this.atk = atk;
  }
}
